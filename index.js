const express = require("express");
const app = express();
const port = process.env.PORT || 5455;

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

app.use(express.json());

app.post("/Appointment", (req, res) => {
  const apptId = (Math.random() * 100000).toFixed(0)

  const result = {
    appointmentId: apptId,
    status: "Scheduled",
    patientId: req.body.sourcePatientId,
    sourceAppointmentId: apptId,
    note: req.body.note,
  };



  res.json({
    ...result,
    jsonResponse: JSON.stringify(result)
  });
});

app.post("/Appointment/Confirm/:appointmentId", (req, res) => {
  const result = {
    externalAppointmentId: req.params.appointmentId,
    status: "Scheduled",
    serviceAppointmentId: req.body.appointmentId
  };

  res.json({
    ...result,
    jsonResponse: JSON.stringify(result)
  });
});

app.post("/Appointment/:appointmentId", (req, res) => {
  const result = {
    externalAppointmentId: req.params.appointmentId,
    status: "Canceled",
    serviceAppointmentId: req.body.appointmentId
  };

  res.json({
    ...result,
    jsonResponse: JSON.stringify(result)
  });
});

app.get("/Slot/:sourceSlotId", (req, res) => {
  console.log(req.params);

  const slot = {};
  slot.slotStart = new Date().toISOString();
  slot.slotEnd = new Date().toISOString();
  slot.slotStatus = "free";
  slot.sourceSlotId = req.params.sourceSlotId;
  slot.sourceSystem = 'Tasy';

  /*if (+Math.random().toFixed(2) < 0.2) {
    res.status(404);

    res.send("Slot não disponível.");

    return;
  }*/

  res.json(slot);
});

app.post("/Slot", (req, res) => {
  console.log(req.body);
  console.log(req.url);
  console.log(req.method);

  const date1 = new Date(req.body.startDate);
  const date2 = new Date(req.body.endDate);
  const diffTime = Math.abs(date2 - date1);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

  const result = {};

  (req.body.careProviders || []).forEach(cp => {
    const slots = [];

    for (let d = 0; d <= diffDays; d++) {
      const now = new Date(req.body.startDate).addDays(d);

      for (let i = 0; i < 12; i++) {
        const hour = (10 + i + '').padStart(2, '0');

        const year = now.getFullYear() + '';
        const month = ((now.getMonth() + 1) + '').padStart(2, '0');
        const day = (now.getDate() + '').padStart(2, '0');

        const slot1 = {};
        slot1.slotStart = year + '-' + month + '-' + day + 'T' + hour + ':00:00.300Z';
        slot1.slotEnd = year + '-' + month + '-' + day + 'T' + hour + ':30:00.300Z';
        slot1.slotStatus = 'free';
        slot1.sourceSlotId = (Math.random() * 100000).toFixed(0);
        slot1.sourceSystem = 'Tasy';
        slot1.duration = 30;
        slot1.price = Math.round(Math.random() * 200);

        const slot2 = {};
        slot2.slotStart = year + '-' + month + '-' + day + 'T' + hour + ':30:00.300Z';
        slot2.slotEnd = year + '-' + month + '-' + day + 'T' + hour + ':59:59.300Z';
        slot2.slotStatus = 'free';
        slot2.sourceSlotId = (Math.random() * 100000).toFixed(0);
        slot2.sourceSystem = 'Tasy';
        slot2.duration = 30;
        slot2.price = Math.round(Math.random() * 200);
        
        if (+Math.random().toFixed(2) < 0.2)
          slots.push(slot1);

        if (+Math.random().toFixed(2) < 0.2)
          slots.push(slot2);
      }
    }

    result[cp.sourceFacilityId + cp.sourcePractitionerId] = slots;
  });

  res.status(200);
  res.json(result);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
